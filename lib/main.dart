import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:flutter/services.dart';
import 'package:proyecto/Menu.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: white,
      ),
      home: MyHomePage(title: 'MyAgendApp'),
    );
  }
}

const MaterialColor white = const MaterialColor(
  0xFF000000,
  const <int, Color>{
    50 : const Color(0xFF000000),
    100: const Color(0xFF000000),
    200: const Color(0xFF000000),
    300: const Color(0xFF000000),
    400: const Color(0xFF000000),
    500: const Color(0xFF000000),
    600: const Color(0xFF000000),
    700: const Color(0xFF000000),
    800: const Color(0xFF000000),
    900: const Color(0xFF000000),
  },
);


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final LocalAuthentication _localAuthentication = LocalAuthentication();
  bool _canCheckBiometric = false;
  String _autorizacion = "Sin autorizacion";
  List<BiometricType> _availableBiometrictType = List<BiometricType> ();

  Future<void>_checkBiometric()async{
    bool canCheckBiometric=false;
    try{
      canCheckBiometric=await _localAuthentication.canCheckBiometrics;
    }on PlatformException catch(g){
      print(g);
    }
    if(!mounted) return;

    setState(() {
      _canCheckBiometric = canCheckBiometric;
    });
  }
  /////////////////////
  Future<void>_getBiometric()async{
    List<BiometricType> listaBiometrico;
    try{
      listaBiometrico = await _localAuthentication.getAvailableBiometrics();
    }on PlatformException catch(g){
      print(g);
    }
    if(!mounted) return;

    setState(() {
      _availableBiometrictType = listaBiometrico;
    });
  }
  /////////////////////
  Future<void>_autorizado()async{
    //String EsAutorizado="No es Autorizado";
    bool EsAutorizado = false;
    try{
      EsAutorizado =await _localAuthentication.authenticateWithBiometrics(
        localizedReason: "Proceso de autenticacion completo",
        useErrorDialogs: true,
        stickyAuth: true,
      );
    }on PlatformException catch(g){
      print(g);
    }
    if(!mounted) return;

    setState(() {
      if(EsAutorizado){
        _autorizacion = "Autorizado";
        Navigator.push(context, MaterialPageRoute(builder: (context)=>Menu()),);
      }else{
        _autorizacion = "Sin autorizacion";
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/img/icon2.png"),
            /*
            Text ("Seleccionar biometrico: $_canCheckBiometric"),
            RaisedButton(onPressed: _checkBiometric,child: Text("Seleccionar Biometrico"),
            color: Colors.red,
              colorBrightness: Brightness.light,
            ),
            Text ("Lista de biometrico: ${_availableBiometrictType.toString()}"),
            RaisedButton(onPressed: _getBiometric,child: Text("Listar Biometrico"),
              color: Colors.red,
              colorBrightness: Brightness.light,
            ),*/

            Text ("$_autorizacion"),
            RaisedButton(onPressed: _autorizado,
              child: Text("Abrir",
              style: TextStyle(fontSize: 22),
              ),
              shape: RoundedRectangleBorder( borderRadius: new BorderRadius.circular(50.0)),
              color: Colors.black,
              colorBrightness: Brightness.dark,
            ),
          ],
        ),
      ),
    );
  }
}
