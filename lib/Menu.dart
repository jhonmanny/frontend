import 'package:flutter/material.dart';
import 'package:proyecto/main.dart';

class Menu extends StatefulWidget{

  @override
  _menu createState() => new _menu();
}

class _menu extends State<Menu>{

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: "Acciones de agenda",
      theme: ThemeData(
        primarySwatch: white,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text("Agenda local"),
          ),
          body: Column(
            children: <MyMenu>[
              MyMenu("1"),
              MyMenu("2"),
              MyMenu("3"),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add_circle_outline),
            backgroundColor: Colors.black,
            onPressed: (){},
          )
      ),
    );
  }
}

class MyMenu extends StatelessWidget{
  String Tareas="Mi tarea";

  MyMenu(this.Tareas);

  @override
  Widget build(BuildContext context) {
    return Card(
        child:Center(
          child: Text(Tareas),
        ),
    );
  }
}